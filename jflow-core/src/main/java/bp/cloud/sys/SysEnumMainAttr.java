package bp.cloud.sys;

import bp.da.*;
import bp.en.*;
import bp.sys.*;

/** 
 sss
 
*/
public class SysEnumMainAttr extends EntityNoNameAttr
{
	/** 
	 配置的值
	 
	*/
	public static final String CfgVal = "CfgVal";
	/** 
	 语言
	 
	*/
	public static final String Lang = "Lang";
	/** 
	 组织结构编码
	 
	*/
	public static final String OrgNo = "OrgNo";
	/** 
	 真实的编号
	 
	*/
	public static final String EnumKey = "EnumKey";
	public static final String IsHaveDtl = "IsHaveDtl";
	public static final String AtPara = "AtPara";
}
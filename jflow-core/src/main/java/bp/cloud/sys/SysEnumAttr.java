package bp.cloud.sys;

/**
 sss
 
*/
public class SysEnumAttr
{
	/** 
	 标题  
	 
	*/
	public static final String Lab="Lab";
	/** 
	 Int key
	 
	*/
	public static final String IntKey="IntKey";
	/** 
	 EnumKey
	 
	*/
	public static final String EnumKey="EnumKey";
	/** 
	 Language
	 
	*/
	public static final String Lang="Lang";
	/** 
	 组织结构编码
	 
	*/
	public static final String OrgNo = "OrgNo";
	/** 
	 RefPK
	 
	*/
	public static final String RefPK = "RefPK";
}